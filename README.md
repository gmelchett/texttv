# SVT Text TV viewer

This is a simple python pygame_sdl2 app for Sailfish OS. It uses the public api from [texttv.nu](http://texttv.nu)
to display Swedish Television teletext.
The application is in Swedish only.

![alt text](screenshot.png "App screenshot")

## Prerequisits

* python3
* pygame_sdl2
* python requests (Install via pip3)

## Usage:
* Swipe up: Return to page 100
* Swipe down: Refresh page
* Swipe left: Previous page
* Swipe right: Next page

A single press somewhere that is not an underlined number will show the settings. There you
can change orientation and allow wifi-only downloads.

## TODO:
Icon, .desktop-file and RPM package

## Bugs:
Sailfish shows up "Unname application not responding" from time to time. I don't know why.


