#!/usr/bin/env python2
# -*- coding: latin-1 -*-
import sys
import os
import time
import re
import json
import getopt
#import html.parser
import HTMLParser

import requests
import pygame_sdl2
pygame_sdl2.import_as_pygame()

import pygame.font
import pygame.time
import pygame.display

WIDTH = 44
HEIGHT = 24
MAX_PAGE_AGE = 15 * 60

debug = False

class TextTVParse(HTMLParser.HTMLParser):
    def __init__(self):
        HTMLParser.HTMLParser.__init__(self)
        self.data = []
        self.endtag = True

    def handle_starttag(self, tag, attrs):
        self.data.append({"tag":tag, "attrs":attrs, "data":None})
        self.endtag = False

    def handle_data(self, d):
        if not self.endtag:
            self.data[-1]["data"] = d
        else:
            self.data.append({"tag":None, "attrs":None, "data":d})

    def handle_endtag(self, tag):
        self.endtag = True

    def read_result(self):
        return self.data


def create_page(page, subpage, settings):
    global default_font, font_h, font_w

    page_img = pygame.Surface(((WIDTH+2) * font_w, HEIGHT * font_h))
    page_img.fill((0, 0, 0))

    parser = TextTVParse()
    parser.feed(page["content"][subpage])

    action_areas = []
    y = 0
    x = 0

    fg = (255, 255, 255)
    bg = (0, 0, 0)
    bold = False
    current_font = default_font

    for t in parser.read_result():
        if t["tag"] == "div":
            continue
        if not t["data"]:
            continue

        m = t["data"]

        if debug and (t["tag"] or t["attrs"] or t["data"]):
            print("tag: %s attrs: %s data:*%s*" % (t["tag"], t["attrs"], t["data"].replace("\n", "\\n")))

        if t["attrs"] and t["attrs"][0][0] == "class":
            if t["attrs"][0][1].startswith("B"):
                fg = (0, 0, 128)
            elif t["attrs"][0][1].startswith("Y"):
                fg = (222, 222, 0)
            elif t["attrs"][0][1].startswith("C"):
                fg = (0, 222, 222)
            elif t["attrs"][0][1].startswith("W"):
                fg = (255, 255, 255)
            elif t["attrs"][0][1].startswith("G"):
                fg = (0, 222, 0)
            elif t["attrs"][0][1].startswith("R"):
                fg = (222, 0, 0)
            elif t["attrs"][0][1].startswith("bg"):
                pass
            elif t["attrs"][0][1] == "toprow":
                pass
            elif t["attrs"][0][1] == "added-line":
                pass
            else:
                if debug:
                    print("*** Unknown attrs: ", t["attrs"])

            if "bgBK" in t["attrs"][0][1]:
                bg = (0, 0, 0)
            elif "bgB" in t["attrs"][0][1]:
                bg = (0, 0, 128)
            elif "bgR" in t["attrs"][0][1]:
                bg = (222, 0, 0)
            elif "bgY" in t["attrs"][0][1]:
                bg = (222, 222, 0)
            elif "bgC" in t["attrs"][0][1]:
                bg = (0, 222, 222)
            elif "bgG" in t["attrs"][0][1]:
                bg = (0, 222, 0)
            elif "bgW" in t["attrs"][0][1]:
                bg = (255, 255, 255)
            elif "bg" in  t["attrs"][0][1]:
                if debug:
                    print("*** Unknown bg:", t["attrs"])

            if "DH" in t["attrs"][0][1]:
                bold = True

        current_font.set_bold(bold)

        if t["tag"] == "a":
            adr = t["attrs"][0][1].strip("/").split(",")
            adr_idx = 0
            for n in re.split("(/)", m):
                if not len(n):
                    continue
                if n == "/":
                    current_font.set_underline(False)
                else:
                    current_font.set_underline(True)
                text = current_font.render(n, True, fg, bg)
                page_img.blit(text, (x, y))
                if n != '/':
                    if not settings["landscape"]:
                        c = {"page":adr[adr_idx], "x":x, "y":y, "h":text.get_height(), "w":text.get_width()}
                    else:
                        c = {"page":adr[adr_idx], "x":page_img.get_height() - y - text.get_height(),
                             "y":x, "h":text.get_width(), "w":text.get_height()}
                    adr_idx += 1
                    action_areas.append(c)

                x += text.get_width()
            continue

        current_font.set_underline(False)

        for n in re.split("(\n)", m):

            if not len(n):
                continue

            text = current_font.render(n, True, fg, bg)

            if n == "\n":
                y += font_h
                x = 0
                fg = (255, 255, 255)
                bg = (0, 0, 0)
                bold = False
                current_font = default_font
                continue

            page_img.blit(text, (x, y))

            x += font_w * len(n)

    return action_areas, page_img

def fetch_page(cache, page_nr):
    if page_nr in cache and time.time() - cache[page_nr]["datetime"] < MAX_PAGE_AGE:
        if debug:
            print("Reusing %s from cache" % page_nr)
        return cache

    if debug:
        print("fetching page: ", page_nr)

    if settings["wifionly"]:
        try:
            wifi = int(open("/sys/class/net/wlan0/carrier").read())
        except IOError:
            wifi = 0
        if not wifi:
            if not page_nr in cache:
                cache[page_nr] = {"content":["<div class=\"root\"><span>\n\n\n\n\n\n\n\n\n\n</span><span class=\"G bgBK\">      Sidan saknas i cachen\n  Ingen wifi uppkoppling finns. </span></div>"],
                                  "next_page":"101","prev_page":"100", "datetime":0}
            return cache

    try:
        r = requests.get("http://api.texttv.nu/api/get/%s?app=texttv" % page_nr)
    except requests.exceptions.ConnectionError:
        if not page_nr in cache:
            cache[page_nr] = {"content":["<div class=\"root\"><span>\n\n\n\n\n\n\n\n\n\n</span><span class=\"R bgBK\">            Ingen internetuppkoppling. </span></div>"],
                              "next_page":"101","prev_page":"100", "datetime":0}
    else:
        cache[page_nr] = r.json()[0]
        for i in range(len(cache[page_nr]["content"])):
            cache[page_nr]["content"][i] = re.sub('<span class="toprow"> (\d+) SVT Text     ',
                                                  '<span class="toprow"> \\1 SVT Text %02d:%02d' % 
                                                  (time.localtime().tm_hour, time.localtime().tm_min), cache[page_nr]["content"][i])

        cache[page_nr]["datetime"] = time.time()
        open(cache_file, "w").write(json.dumps(cache, sort_keys=True, indent=4))
    return cache


def generate_settings_page(cache, settingsa):

    settings_txt = {}

    for k in settings:
        if settings[k]:
            settings_txt[k] = "Ja"
        else:
            settings_txt[k] = "Nej"

    cache["settings"] = {"content":["<div class=\"root\"><span>\n</span>"
                                     u'<h1 class="Y DH">Inst�llningar:</h1>\n\n'
                                     "<span class=\"W bgBK\">Liggande:                        [</span><a href=\"/landscape\">%s</a><span>]\n\n</span>"
                                     u"<span class=\"W bgBK\">Nerladdning enbart �ver wifi:    [</span><a href=\"/wifionly\">%s</a><span>]\n\n</span>"
                                     "<a href=\"/done\">Klart</a><span>\n\n</span>"
                                     '<h1 class="Y DH">Annat:</h1>\n\n'
                                     "<a href=\"/cleancache\">Rensa cachen</a><span>\n\n</span>"
                                     "<a href=\"/quit\">Avsluta</a><span>\n\n</span>"
                                     "</div>" %
                          (settings_txt["landscape"], settings_txt["wifionly"])],
                          "next_page":"101","prev_page":"100"}

    return cache

def display_page(cache, screen, settings, page_nr, subpage):

    action_areas, page_img = create_page(cache[page_nr], subpage, settings)

    if settings["landscape"]:
        page_img = pygame.transform.rotate(page_img, 270)
    screen.blit(page_img, (0,0))

    pygame.display.flip()
    return action_areas


if __name__ == "__main__":
    global default_font, font_w, font_h, cache_file

    configpath = os.getenv("XDG_CONFIG_HOME")

    if not configpath:
        configpath = os.path.join(os.getenv("HOME"), ".config")

    datapath = os.getenv("XDG_DATA_HOME")
    if not datapath:
        datapath = os.path.join(os.getenv("HOME"), ".local", "share")

    configpath = os.path.join(configpath, "texttv")
    datapath = os.path.join(datapath, "texttv")
    try:
        os.makedirs(configpath)
    except OSError:
        pass

    try:
        os.makedirs(datapath)
    except OSError:
        pass

    cache_file = os.path.join(datapath, "cache.json")
    settings_file = os.path.join(configpath, "settings.json")

    try:
        settings = json.loads(open(settings_file).read())
    except:
        settings = {}
        settings["landscape"] = False
        settings["wifionly"] = False

    try:
        cache = json.loads(open(cache_file).read())
    except IOError:
        cache = {}
    curr_subpage = 0
    page_nr = "100"
    fullscreen = False

    options, remainder = getopt.getopt(sys.argv[1:], 'p:s:f', ['page=',
                                                               'fullscreen',
                                                               'subpage=',
    ])

    for opt, arg in options:
        if opt in ('-p', '--page'):
            page_nr = arg
        elif opt in ('-s', '--subpage'):
            curr_subpage = int(arg) - 1
        elif opt in ('-f', "--fullscreen"):
            fullscreen = True

    pygame.init()

    cache = generate_settings_page(cache, settings)
    cache = fetch_page(cache, page_nr)

    if cache[page_nr]["next_page"]:
        cache = fetch_page(cache, cache[page_nr]["next_page"])

    di = pygame.display.Info()

    if fullscreen:
        flags = pygame.FULLSCREEN
    else:
        flags = 0

    screen = pygame.display.set_mode((di.current_w, di.current_h), flags)

    fsize = 8
    
    font = pygame.font.SysFont("liberationmono,wenquanyizenheimono,dejavusansmono,droidsansmono", fsize)
    (font_w, font_h) = font.size("A")

    while (font_w * (WIDTH+3)) < screen.get_width() and (font_h * HEIGHT < screen.get_height()):
        prev_font = font
        font = pygame.font.SysFont("liberationmono,wenquanyizenheimono,dejavusansmono,droidsansmono", fsize)
        (font_w, font_h) = font.size("A")
        fsize += 1

    default_font = prev_font
    (font_w, font_h) = default_font.size("A")

    done = False

    action_areas = display_page(cache, screen, settings, page_nr, curr_subpage)

    while not done:
        event = pygame.event.wait()

        if event.type == pygame.QUIT:
            done = True
            continue
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            done = True
            continue
        elif event.type == pygame.FINGERDOWN:
            start_move = event

        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 or event.type == pygame.FINGERUP:

            if event.type == pygame.FINGERUP:

                if abs(start_move.y - event.y) > 250 or abs(start_move.x - event.x) > 250:
                    # Swipe movement
                    left = False
                    right = False
                    up = False
                    down = False

                    if start_move.x - event.x < 0 and abs(start_move.y - event.y) / abs(start_move.x - event.x) < 0.2:
                        right = True
                    if start_move.x - event.x > 0 and abs(start_move.y - event.y) / abs(start_move.x - event.x) < 0.2:
                        left = True
                    if start_move.y - event.y < 0 and abs(start_move.x - event.x) / abs(start_move.y - event.y) < 0.2:
                        down = True
                    if start_move.y - event.y > 0 and abs(start_move.x - event.x) / abs(start_move.y - event.y) < 0.2:
                        up = True

                    if not left and not right and not up and not down:
                        continue

                    if settings["landscape"]:
                        tmp = right
                        right = down
                        tmp2 = left
                        left = up
                        up = tmp
                        down = tmp2

                    if right:
                        if len(cache[page_nr]["content"]) > 1:
                            curr_subpage += 1
                            if curr_subpage < len(cache[page_nr]["content"]):
                                action_areas = display_page(cache, screen, settings, page_nr, curr_subpage)
                                continue
                            curr_subpage = 0

                        if "next_page" in cache[page_nr]:
                            page_nr = cache[page_nr]["next_page"]
                        else:
                            page_nr = "100"

                    if left:
                        if len(cache[page_nr]["content"]) > 1:
                            curr_subpage -= 1
                            if curr_subpage >= 0:
                                action_areas = display_page(cache, screen, settings, page_nr, curr_subpage)
                                continue
                            curr_subpage = 0

                        if "prev_page" in cache[page_nr]:
                            page_nr = cache[page_nr]["prev_page"]
                        else:
                            page_nr = "100"

                    if up:
                        page_nr = "100"
                    if down:
                        # Force refersh
                        cache[page_nr]["datetime"] = 0

                    cache = fetch_page(cache, page_nr)
                    action_areas = display_page(cache, screen, settings, page_nr, curr_subpage)
                    continue

                pos_x = event.x
                pos_y = event.y
            else:
                pos_x = event.pos[0]
                pos_y = event.pos[1]
            for a in action_areas:
                if pos_x >= a["x"] and pos_x < (a["x"] + a["w"]) and pos_y >= a["y"] and pos_y < (a["y"] + a["h"]):
                    page_nr = a["page"]

                    if page_nr == "landscape" or page_nr == "wifionly":
                        settings[page_nr] = not settings[page_nr]
                        cache = generate_settings_page(cache, settings)
                        action_areas = display_page(cache, screen, settings, "settings", 0)
                        break
                    elif page_nr == "done":
                        open(settings_file, "w").write(json.dumps(settings, sort_keys=True, indent=4))
                        curr_subpage = 0
                        page_nr = "100"
                    elif page_nr == "quit":
                        done = True
                        break
                    elif page_nr == "cleancache":
                        cache = {}
                        open(cache_file, "w").write(json.dumps(cache, sort_keys=True, indent=4))
                        break
                    
                    cache = fetch_page(cache, page_nr)
                    curr_subpage = 0
                    action_areas = display_page(cache, screen, settings, page_nr, curr_subpage)
                    break
            else:
                action_areas = display_page(cache, screen, settings, "settings", 0)
